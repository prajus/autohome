const int ANALOG_START = 20;
const int ANALOG_PIN_COUNT = 6;

char pin_status[ANALOG_START];
int  pin_thresholds[ANALOG_PIN_COUNT];

bool USER_ACTIVE = true;

const int LDR_PIN = A0;
const int TEMP_PIN = A1;

const int PIN_ID_A0 = 20;
const int PIN_ID_A1 = 21;
const int PIN_ID_A2 = 22;
const int PIN_ID_A3 = 23;
const int PIN_ID_A4 = 24;
const int PIN_ID_A5 = 25;

const int RETRY_COUNT = 20;
const int DEVICE_ID = 0x1;

const int MODE_AUTO = 0x1;
const int MODE_MANUAL = 0x2;

const int LIGHT_DB = 0x1;
const int TEMP_DB = 0x2;
const int ACCESS_DB = 0x3;

const int OP_CODE_DISC = 255;
const int OP_CODE_ACK  = 254;

const int OP_CODE_DEVICE_ID = 1;
const int OP_CODE_CONFIG = 2;
const int OP_CODE_STATUS = 3;
const int ACCESS_DB_OP_CODE_USER_PRESENCE = 8;

const int NUM_LIGHT_PINS = 4;

const int AC_PIN = 6;
const int lighting_pins[NUM_LIGHT_PINS] = {2,3,4,5};
const int light_thresh = 350;
int ligthing_mode[ANALOG_START];

int read_temp (int pin)
{
    int value = 0;

    analogRead(pin);
    delay(1);
    value = (5.0 * analogRead(pin) * 100.0) / 1024;
    //temp calliberation
    value = value - 8;

    return(value);
}

void monitor_temp ()
{
    int value = 0;
    int thresh_index = 0;

    // sense the LM35
    value = read_temp(TEMP_PIN);

    thresh_index = get_pin_thres_offset(TEMP_PIN);

    if ((value > pin_thresholds[thresh_index]) && USER_ACTIVE) {
        digitalWrite(AC_PIN, HIGH);
        pin_status[AC_PIN] = 1;
    } else {
        digitalWrite(AC_PIN, LOW);
        pin_status[AC_PIN] = 0;
    }
}

void monitor_lights ()
{
    int i, num_auto = 0;
    int value = 0;
    bool auto_req = false;
    int  auto_pins[NUM_LIGHT_PINS] = {0};
    int  pin_number = 0;

    for (i = 0; i < NUM_LIGHT_PINS; i++) {

        if (ligthing_mode[lighting_pins[i]] == MODE_AUTO) {
            auto_req = true;
            auto_pins[num_auto++] = i;
        }
    }

    if (!auto_req) {
        return;
    }

    // sense the LDR
    analogRead(LDR_PIN);
    delay(1);
    value = (5.0 * analogRead(LDR_PIN) * 100.0) / 1024;

    if (value > light_thresh && USER_ACTIVE) {
        // switch on all auto lights
        for (i = 0; i < num_auto; i++) {
            digitalWrite(lighting_pins[auto_pins[i]], HIGH);
            pin_number = lighting_pins[auto_pins[i]];
            pin_status[pin_number] = 1;
        }
    } else {
        // switch off all auto loghts
        for (i = 0; i < num_auto; i++) {
            digitalWrite(lighting_pins[auto_pins[i]], LOW);
            pin_number = lighting_pins[auto_pins[i]];
            pin_status[pin_number] = 0;
        }
    }
}

int get_arduino_pin (int pin_id)
{
    if (pin_id < ANALOG_START) {
        return (pin_id);
    } else {
        switch (pin_id) {
            case PIN_ID_A0:
                return (A0);
            case PIN_ID_A1:
                return (A1);
            case PIN_ID_A2:
                return (A2);
            case PIN_ID_A3:
                return (A3);
            case PIN_ID_A4:
                return (A4);
            case PIN_ID_A5:
                return (A5);
        }
    }
}

int get_pin_thres_offset (int pin_id)
{
        switch (pin_id) {
            case PIN_ID_A0:
                return (0);
            case PIN_ID_A1:
                return (1);
            case PIN_ID_A2:
                return (2);
            case PIN_ID_A3:
                return (3);
            case PIN_ID_A4:
                return (4);
            case PIN_ID_A5:
                return (5);
        }
}

void send_status (void)
{
    int incomingByte = 0;
    int data_valid = 0;
    int pin_number = -1;
    int pin_id = -1;
    int retry = 0;
    int value = 0;
    int db_id = 0;
    int mode = 0;
    int thresh_index = 0;

    for (retry = 0; retry < RETRY_COUNT; retry++) {
        delay(1);
        if (Serial.available()) {

            incomingByte = Serial.read();

            if (incomingByte == OP_CODE_DISC) {
                // discovery message, NOP
            } else {
                data_valid = 1;
                db_id = incomingByte;
                break;
            }
        }
    }

    if (!data_valid) {
        return;
    }

    switch (db_id) {

        case LIGHT_DB: {

            for (retry = 0; retry < RETRY_COUNT; retry++) {

                delay(1);

                if (Serial.available()) {

                    incomingByte = Serial.read();

                    if (incomingByte == OP_CODE_DISC) {
                        // discovery message, NOP
                    } else {
                        data_valid = 1;
                        pin_id = incomingByte;
                        break;
                    }
                }
            }

            if (!data_valid) {
                return;
            }

            pin_number = get_arduino_pin(pin_id);

            mode = ligthing_mode[pin_number];

            value = pin_status[pin_number];

            delay(1);
            Serial.println(OP_CODE_ACK, DEC);

            delay(1);
            Serial.println(mode, DEC);

            delay(1);
            Serial.println(value, DEC);

            break;
        }

        case TEMP_DB: {

            // send ack for query request
            delay(1);
            Serial.println(OP_CODE_ACK, DEC);

            // send configured temp
            delay(1);
            thresh_index = get_pin_thres_offset(TEMP_PIN);
            value = pin_thresholds[thresh_index];
            Serial.println(value, DEC);

            // send current temp
            delay(1);
            value = read_temp(TEMP_PIN);
            Serial.println(value, DEC);

            // send ac status
            delay(1);
            value = pin_status[AC_PIN];
            Serial.println(value, DEC);

            break;
        }


        default:
            break;
    }


    delay(1);
    Serial.println(OP_CODE_ACK, DEC);

}

void configure_sensors (void)
{
    int incomingByte = 0;
    int data_valid = 0;
    int pin_number = -1;
    int pin_id = -1;
    int retry = 0;
    int value = 0;
    int pin_thresh_id = 0;
    int db_id = 0;
    int mode = 0;
    int access_db_opcode = 0;
    int user_available = 0;

    for (retry = 0; retry < RETRY_COUNT; retry++) {
        delay(1);
        if (Serial.available()) {

            incomingByte = Serial.read();

            if (incomingByte == OP_CODE_DISC) {
                // discovery message, NOP
            } else {
                data_valid = 1;
                db_id = incomingByte;
                break;
            }
        }
    }

    if (!data_valid) {
        Serial.println(OP_CODE_ACK, DEC);
        return;
    }

    switch (db_id) {

        case LIGHT_DB: {

            for (retry = 0; retry < RETRY_COUNT; retry++) {

                delay(1);

                if (Serial.available()) {

                    incomingByte = Serial.read();

                    if (incomingByte == OP_CODE_DISC) {
                        // discovery message, NOP
                    } else {
                        data_valid = 1;
                        pin_id = incomingByte;
                        break;
                    }
                }
            }

            if (!data_valid) {
                Serial.println(OP_CODE_ACK, DEC);
                return;
            }

            pin_number = get_arduino_pin(pin_id);

            data_valid = 0;

            for (retry = 0; retry < RETRY_COUNT; retry++) {

                delay(1);

                if (Serial.available()) {

                    incomingByte = Serial.read();

                    if (incomingByte == OP_CODE_DISC) {
                        // discovery message, NOP
                    } else {
                        data_valid = 1;
                        mode = incomingByte;
                        break;
                    }
                }
            }

            if (!data_valid) {
                Serial.println(OP_CODE_ACK, DEC);
                return;
            }

            data_valid = 0;

            for (retry = 0; retry < RETRY_COUNT; retry++) {

                delay(1);

                if (Serial.available()) {

                    incomingByte = Serial.read();

                    if (incomingByte == OP_CODE_DISC) {
                        // discovery message, NOP
                    } else {
                        data_valid = 1;
                        value = incomingByte;
                        break;
                    }
                }
            }

            if (!data_valid) {
                Serial.println(OP_CODE_ACK, DEC);
                return;
            }

            if (mode == MODE_AUTO) {
                ligthing_mode[pin_number] = MODE_AUTO;
            } else {

                ligthing_mode[pin_number] = MODE_MANUAL;
                if (value) {
                    digitalWrite(pin_number, HIGH);
                } else {
                    digitalWrite(pin_number, LOW);
                }
                pin_status[pin_number] = value;
            }

            break;
        }

        case TEMP_DB: {

            for (retry = 0; retry < RETRY_COUNT; retry++) {

                delay(1);

                if (Serial.available()) {

                    incomingByte = Serial.read();

                    if (incomingByte == OP_CODE_DISC) {
                        // discovery message, NOP
                    } else {
                        data_valid = 1;
                        value = incomingByte;
                        break;
                    }
                }
            }

            if (!data_valid) {
                Serial.println(OP_CODE_ACK, DEC);
                return;
            }

            pin_thresh_id = get_pin_thres_offset(TEMP_DB);

            pin_thresholds[pin_thresh_id] = value;

            break;
        }
        
        case ACCESS_DB: {

          for (retry = 0; retry < RETRY_COUNT; retry++) {
              delay(1);
              if (Serial.available()) {

                  incomingByte = Serial.read();

                  if (incomingByte == OP_CODE_DISC) {
                      // discovery message, NOP
                  } else {
                      data_valid = 1;
                      access_db_opcode = incomingByte;
                      break;
                  }
              }
          }

          if (!data_valid) {
              Serial.println(OP_CODE_ACK, DEC);
              return;
          }
          
          switch (access_db_opcode) {

            case ACCESS_DB_OP_CODE_USER_PRESENCE: {

              //Serial.println("deactivate key");

             for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {

                      incomingByte = Serial.read();

                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          user_available  = incomingByte;
                          break;
                      }
                  }
              }

              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }

              USER_ACTIVE = (user_available ? true : false);

              break;
            }

            default:
              break;
          }

          break;
        }        

        default:
            break;
    }

    delay(1);
    Serial.println(OP_CODE_ACK, DEC);
}

void get_device_id (void)
{
    delay(1);
    Serial.println(DEVICE_ID, DEC);
    delay(1);
    Serial.println(OP_CODE_ACK, DEC);
}


void read_serial_data (void)
{

    int incomingByte = 0;

    if (Serial.available()) {

        incomingByte = Serial.read();

        switch (incomingByte) {

            case OP_CODE_DISC:
                // discovery message send back discovery opcode
                Serial.println(OP_CODE_DISC, DEC);
                break;

            case OP_CODE_STATUS:
                // get the status of the pins
                send_status();
                break;

            case OP_CODE_CONFIG:
                // configure pins
                configure_sensors();
                break;

            case OP_CODE_DEVICE_ID:
                // configure pins
                get_device_id();
                break;

           default:
                 Serial.println("unexpected");
                 Serial.println(incomingByte, DEC);
                 break;

        }
      }
}

void setup() {
  int i;
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  Serial.begin(9600);

  for (i = 0; i < ANALOG_START; i++) {
      ligthing_mode[i] = MODE_AUTO;
  }

}

void loop() {

  read_serial_data();
  monitor_lights();
  monitor_temp();

  delay(10);
}
