#include <SPI.h>
#include </autohome/exec/arduino/MFRC522.h>

#define RST_PIN         9
#define SS_PIN          10
#define MAX_KEY_SIZE    12
#define MAX_KEY         10

const int RETRY_COUNT = 20;
const int SCAN_COUNT = 60;
const int SCAN_DELAY = 2000;

const int DEVICE_ID = 0x2;

const int ACCESS_DB = 0x3;

const int OP_CODE_DISC = 255;
const int OP_CODE_ACK  = 254;

const int OP_CODE_DEVICE_ID = 1;
const int OP_CODE_CONFIG = 2;
const int OP_CODE_STATUS = 3;

const int ACCESS_PERMIT_LED = 3;
const int ACCESS_DENY_LED = 4;
const int ACCESS_BUZZER=2;

const int ACCESS_DB_OPCODE_SENSE = 0x4;
const int ACCESS_DB_OPCODE_ACTIVATE_KEY = 0x5;
const int ACCESS_DB_OPCODE_DEACTIVATE_KEY = 0x6;
const int ACCESS_DB_OPCODE_SYNC = 0x7;
const int ACCESS_DB_OPCODE_DELETE= 0x9;

uint8_t scan_count = 0;

MFRC522 mfrc522(SS_PIN, RST_PIN);

typedef struct access_key_t_ {
  uint8_t key_id;
  bool key_status;
  bool key_valid;
  bool key_active;
  uint8_t key_size;
  uint8_t key[MAX_KEY_SIZE];
} access_key_t;

access_key_t access_key[MAX_KEY];

void send_status (void)
{
    int incomingByte = 0;
    int data_valid = 0;
    int db_id = 0;
    int retry;
    int key_index = 0;

    for (retry = 0; retry < RETRY_COUNT; retry++) {
        delay(1);
        if (Serial.available()) {

            incomingByte = Serial.read();

            if (incomingByte == OP_CODE_DISC) {
                // discovery message, NOP
            } else {
                data_valid = 1;
                db_id = incomingByte;
                break;
            }
        }
    }

    if (!data_valid) {
        return;
    }

    switch (db_id) {
      
        case ACCESS_DB: {

            for (retry = 0; retry < RETRY_COUNT; retry++) {

                delay(1);

                if (Serial.available()) {

                    incomingByte = Serial.read();

                    if (incomingByte == OP_CODE_DISC) {
                        // discovery message, NOP
                    } else {
                        data_valid = 1;
                        key_index = incomingByte;
                        break;
                    }
                }
            }

            if (!data_valid) {
                return;
            }

            delay(1);
            Serial.println(OP_CODE_ACK, DEC);
            
            delay(1);
            Serial.println(access_key[key_index].key_valid, DEC);

            delay(1);
            Serial.println(access_key[key_index].key_active, DEC);
            
            delay(1);
            Serial.println(access_key[key_index].key_status, DEC);

            break;
        }      

        default:
            break;
    }


    delay(1);
    Serial.println(OP_CODE_ACK, DEC);

}

void configure_sensors (void)
{
    int incomingByte = 0;
    int data_valid = 0;
    int db_id = 0;
    int retry;
    int i;
    int key_size;
    int key;
    int key_active;
    uint8_t key_ar[MAX_KEY_SIZE];
    int access_db_opcode;
    bool card_detected = false;
    uint8_t key_index = -1; 

    for (retry = 0; retry < RETRY_COUNT; retry++) {
        delay(1);
        if (Serial.available()) {

            incomingByte = Serial.read();

            if (incomingByte == OP_CODE_DISC) {
                // discovery message, NOP
            } else {
                data_valid = 1;
                db_id = incomingByte;
                break;
            }
        }
    }

    if (!data_valid) {
        Serial.println(OP_CODE_ACK, DEC);
        return;
    }

    switch (db_id) {

        case ACCESS_DB: {
          
          
           for (retry = 0; retry < RETRY_COUNT; retry++) {
              delay(1);
              if (Serial.available()) {
      
                  incomingByte = Serial.read();
      
                  if (incomingByte == OP_CODE_DISC) {
                      // discovery message, NOP
                  } else {
                      data_valid = 1;
                      access_db_opcode = incomingByte;
                      break;
                  }
              }
          }
      
          if (!data_valid) {
              Serial.println(OP_CODE_ACK, DEC);
              return;
          }         
          
          switch (access_db_opcode) {
            
            case ACCESS_DB_OPCODE_SENSE: {
              
              delay(1);
              Serial.println(OP_CODE_ACK, DEC);
              
              for (retry = 0; retry < SCAN_COUNT; retry++) {
                  
                delay(SCAN_DELAY);
                
                card_detected = scan_for_cards(true, &key_index);
                
                if (card_detected) {         
                  break;
                }
                
              }
              
              if (card_detected) {
                //send the new card details
                Serial.println(true, DEC);
                Serial.println(key_index, DEC);
                key_size = access_key[key_index].key_size;
                Serial.println(key_size, DEC);
                
                for (i = 0; i < key_size; i++) {
                  key = access_key[key_index].key[i];
                  Serial.println(key, DEC);
                }
              } else {
                Serial.println(false, DEC);
              }
              
              break;
            }
            
            case ACCESS_DB_OPCODE_ACTIVATE_KEY: {
              
               //Serial.println("activate key");
               for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {
          
                      incomingByte = Serial.read();
          
                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          key_index = incomingByte;
                          break;
                      }
                  }
              }
          
              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }    
   
              //Serial.println(key_index, DEC);           
            
              access_key[key_index].key_active = true;
              
              break;
            }
            
            case ACCESS_DB_OPCODE_DEACTIVATE_KEY: { 
              
              //Serial.println("deactivate key"); 
 
             for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {
          
                      incomingByte = Serial.read();
          
                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          key_index = incomingByte;
                          break;
                      }
                  }
              }
          
              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }               
            
              access_key[key_index].key_active = false;
              
              break;
            }
       
           case ACCESS_DB_OPCODE_SYNC: { 
              
              //Serial.println("deactivate key"); 
 
             for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {
          
                      incomingByte = Serial.read();
          
                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          key_index = incomingByte;
                          break;
                      }
                  }
              }
          
              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }               
            
             for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {
          
                      incomingByte = Serial.read();
          
                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          key_size = incomingByte;
                          break;
                      }
                  }
              }
          
              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }      
       
              for (i = 0; i < key_size; i++) {

               for (retry = 0; retry < RETRY_COUNT; retry++) {
                    delay(1);
                    if (Serial.available()) {
            
                        incomingByte = Serial.read();
            
                        if (incomingByte == OP_CODE_DISC) {
                            // discovery message, NOP
                        } else {
                            data_valid = 1;
                            key_ar[i] = incomingByte;
                            break;
                        }
                    }
                }
            
                if (!data_valid) {
                    Serial.println(OP_CODE_ACK, DEC);
                    return;
                }                  
                
              }                         
            
             for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {
          
                      incomingByte = Serial.read();
          
                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          key_active = incomingByte;
                          break;
                      }
                  }
              }
          
              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }             
            
              memset(&access_key[key_index], 0, sizeof(access_key_t));
              
              access_key[key_index].key_valid = true;
              access_key[key_index].key_status = false; // assume user is away
              access_key[key_index].key_active = (key_active ? true : false);
              access_key[key_index].key_size = key_size;
              
              for (i = 0; i < key_size; i++) {
                access_key[key_index].key[i] = key_ar[i];
              }
              
              break;
            }        
       
           case ACCESS_DB_OPCODE_DELETE: { 
              
              //Serial.println("deactivate key"); 
 
             for (retry = 0; retry < RETRY_COUNT; retry++) {
                  delay(1);
                  if (Serial.available()) {
          
                      incomingByte = Serial.read();
          
                      if (incomingByte == OP_CODE_DISC) {
                          // discovery message, NOP
                      } else {
                          data_valid = 1;
                          key_index = incomingByte;
                          break;
                      }
                  }
              }
          
              if (!data_valid) {
                  Serial.println(OP_CODE_ACK, DEC);
                  return;
              }               
            
              memset(&access_key[key_index], 0, sizeof(access_key_t));
              
              break;
            }      
            
            default:
              break;
          }
          
          break;
        }
        
        default:
            break;
    }

    delay(1);
    Serial.println(OP_CODE_ACK, DEC);
}

void get_device_id (void)
{
    delay(1);
    Serial.println(DEVICE_ID, DEC);
    delay(1);
    Serial.println(OP_CODE_ACK, DEC);
}


void read_serial_data (void)
{

    int incomingByte = 0;

    if (Serial.available()) {

        incomingByte = Serial.read();

        switch (incomingByte) {

            case OP_CODE_DISC:
                // discovery message send back discovery opcode
                Serial.println(OP_CODE_DISC, DEC);
                break;

            case OP_CODE_STATUS:
                // get the status of the pins
                send_status();
                break;

            case OP_CODE_CONFIG:
                // configure pins
                configure_sensors();
                break;

            case OP_CODE_DEVICE_ID:
                // configure pins
                get_device_id();
                break;

           default:
                 Serial.println("unexpected");
                 Serial.println(incomingByte, DEC);
                 break;

        }
      }
}

void dump_byte_array(byte *buffer, byte bufferSize)
{
  int i;
  
  //Serial.print("detected");
  
  //Serial.println(bufferSize, DEC);
  
  for (i = 0; i < bufferSize; i++) {
    //Serial.println(buffer[i]);
  }
  
  //Serial.print("end");
}

bool add_or_validate_access_card(uint8_t *byte, 
                                 uint8_t size, 
                                 bool add_key, 
                                 uint8_t *key_index)
{
  int keys = 0;
  int i = 0;
  bool key_found = false;
  bool key_added = false;
  bool mismatch = false;
  
  if (!size) {
    return (false);
  }
  
  for (keys = 0; keys < MAX_KEY; keys++) {
    
    if (!access_key[keys].key_valid) {
      continue;
    }
    
    if (access_key[keys].key_size == size) {
      
      mismatch = false;
      
      for (i = 0; i < size; i++) {
        
        if (access_key[keys].key[i] != byte[i]) {
          mismatch = true;
          break;
        }
      }
      
      if (!mismatch) {
        key_found = true;
        *key_index = keys;
        break;
      }
      
    } 
    
  }

  if (!key_found && add_key) {
 
   for (keys = 0; keys < MAX_KEY; keys++) {
    
    if (access_key[keys].key_valid) {
      continue;
    }   
    
    access_key[keys].key_valid = true;
    access_key[keys].key_size = size;
    
    for (i = 0; i < size; i++) {   
        access_key[keys].key[i] = byte[i];
    }
   
    key_added = true;
    *key_index = keys;
    break;  
   }
 }
 
 if (add_key) {
   return (key_added);
 } else {
  return (key_found);
 }
 
}

bool scan_for_cards (bool add_key, uint8_t *key_index)
{
  bool status = false;
  bool permit = false;
  bool key_active = false;
  bool user_inside = false;
  //Serial.println("Hi Pi");
  
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return(false);
  }

  //Serial.println("new card present");
  
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return(false);
  }
    
  //Serial.println("card read");
  
  //mfrc522.PICC_DumpToSerial(&mfrc522.uid);
  //dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  
  status = add_or_validate_access_card(mfrc522.uid.uidByte, 
                                       mfrc522.uid.size,
                                       add_key, key_index);
  
  //Serial.println("scan"); 
  //Serial.println(status, DEC); 
  //Serial.println(*key_index, DEC); 
  
  if (!add_key) {
    
     user_inside = access_key[*key_index].key_status;
     
     if (!status) {
       
       permit = false;
       
     } else {
       
       key_active = access_key[*key_index].key_active;
       
       if (key_active) {
         permit = true;
       } 
    }
   
    //Serial.println(permit, DEC);
    //Serial.println("scan end");
  
    if (!user_inside && permit) {
      access_key[*key_index].key_status = true; // let user in
    } else if (user_inside) {
      // always let the user out
      access_key[*key_index].key_status = false; // let user in
    }
    
    if (user_inside) {
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(100);
        digitalWrite(ACCESS_BUZZER, LOW);
    } else {  
      if (permit) {
        digitalWrite(ACCESS_PERMIT_LED, HIGH);
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(100);
        digitalWrite(ACCESS_BUZZER, LOW);
        delay(100);
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(100);
        digitalWrite(ACCESS_BUZZER, LOW);
        digitalWrite(ACCESS_PERMIT_LED, LOW);
      } else {
        digitalWrite(ACCESS_DENY_LED, HIGH);
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(50);
        digitalWrite(ACCESS_BUZZER, LOW);
        delay(50);  
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(50);
        digitalWrite(ACCESS_BUZZER, LOW);
        delay(50); 
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(50);
        digitalWrite(ACCESS_BUZZER, LOW);
        delay(50);  
        digitalWrite(ACCESS_BUZZER, HIGH);
        delay(50);
        digitalWrite(ACCESS_BUZZER, LOW);
        delay(50);         
        digitalWrite(ACCESS_DENY_LED, LOW);
      }
    }
    
  } else {
    // new key getting added 
    digitalWrite(ACCESS_BUZZER, HIGH);
    delay(100);
    digitalWrite(ACCESS_BUZZER, LOW);  
   
    // user is always inside in this case. mark him as inside. 
    access_key[*key_index].key_status = true; // let user in
  }
  
  return (status);
  
}

void setup()
{
  SPI.begin();
  Serial.begin(9600);
  pinMode(ACCESS_PERMIT_LED, OUTPUT);
  pinMode(ACCESS_DENY_LED, OUTPUT);
  pinMode(ACCESS_BUZZER, OUTPUT);
  mfrc522.PCD_Init();
}

void loop() 
{
  uint8_t key_index;
  read_serial_data();
  if (scan_count >= 100) {
    scan_count = 0;
    scan_for_cards(false, &key_index);
  }
  scan_count++;
  delay(10);
}


