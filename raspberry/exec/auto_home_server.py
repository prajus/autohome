#!/usr/bin/python3

import sys
import serial
import time
import threading
import time
import sqlite3
import json
import os
import glob
import json

conn        = dict()
device_map  = dict()

CONST   = dict()
OPCODE  = dict()

conn['path']    = dict()
conn['devid']   = dict()

CONST['RETRY_COUNT']    = int(5)
CONST['LIGHT_DB']       = int(1)
CONST['TEMP_DB']        = int(2)
CONST['ACCESS_DB']      = int(3)
CONST['MODE_AUTO']      = int(1)
CONST['MODE_MANUAL']    = int(2)

OPCODE['DEVICE_ID']     = int(1)
OPCODE['CONFIGURE']     = int(2)
OPCODE['STATUS']        = int(3)
OPCODE['ACCESS_DB_SENSE'] = int(4)
OPCODE['ACCESS_DB_ACTIVATE'] = int(5)
OPCODE['ACCESS_DB_DEACTIVATE'] = int(6)
OPCODE['ACCESS_DB_SYNC'] = int(7)
OPCODE['ACCESS_DB_USER_PRESENCE'] = int(8)
OPCODE['ACCESS_DB_DELETE'] = int(9)
OPCODE['DISCOVERY']     = int(255)
OPCODE['ACK']           = int(254)

def notify_nodes_of_user_entry (sensor_node):
    node_id = 1

    try:
        cmd_list = []
        cmd_list.append(CONST['ACCESS_DB'])
        cmd_list.append(OPCODE['ACCESS_DB_USER_PRESENCE'])
        cmd_list.append(int(1))

        sensor_node.configure_arduino(node_id, cmd_list)

    except Exception as e:
        print("error %s" %e)

def notify_nodes_of_user_exit (sensor_node):
    node_id = 1

    try:

        cmd_list = []
        cmd_list.append(CONST['ACCESS_DB'])
        cmd_list.append(OPCODE['ACCESS_DB_USER_PRESENCE'])
        cmd_list.append(int(0))

        sensor_node.configure_arduino(node_id, cmd_list)

    except Exception as e:
        print("error %s" %e)

def get_num_active_occupants (db):

    try:

        count = int(0)

        db_cmd = "SELECT ID from ACCESS_DB where USER_STATUS='1'"

        cursor = db.execute(db_cmd)

        for row in cursor:
            count = count + 1

        return count    

    except Exception as e:
        print("error %s" %e)



def server_init():

    try:

        global device_map

        with open('/autohome/db/device_map.json') as json_data_file:
            device_map = json.load(json_data_file)

    except Exception as e:
        print("error %s" %e)

def db_init (node_id):

    db_path = "/autohome/db/autoHome.db"

    try:
        db = sqlite3.connect(db_path)

        if (node_id == 1):
            # light/temp db node restarted 
            db_cmd = "UPDATE LIGHT_DB set UPDATED='1'"
            cursor = db.execute(db_cmd)
            db.commit()
            db_cmd = "UPDATE TEMP_DB set UPDATED='1'"
            cursor = db.execute(db_cmd)

            # notify all the access DB details which are not in scan phase
            db.commit()
            db_cmd = "UPDATE ACCESS_DB set UPDATED='1',NOTIFY_DB='1' where LEARN_KEY='0'"
            cursor = db.execute(db_cmd)
            db.commit()
        elif (node_id == 2):
            # access key node restarted

            # mark all tables to be updated, some goes for key scan, some updates the states
            db_cmd = "UPDATE ACCESS_DB set UPDATED='1'"
            cursor = db.execute(db_cmd)

            # sync all the access DB details which are not in scan phase
            db.commit()
            db_cmd = "UPDATE ACCESS_DB set SYNC='1' where LEARN_KEY='0'"
            cursor = db.execute(db_cmd)
            db.commit()

        db.close()

    except Exception as e:
        print("error %s" %e)


def serial_init (port, sensor_node):

    global conn

    print("discover %s" %port)

    try:
        ser = serial.Serial()
        ser.port = port
        ser.baudrate = 9600
        ser.bytesize = serial.EIGHTBITS     #number of bits per bytes
        ser.parity = serial.PARITY_NONE     #set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE  #number of stop bits
        #ser.timeout = None                 #block read
        ser.timeout = 1                     #non-block read
        #ser.timeout = 2                    #timeout block read
        ser.xonxoff = False                 #disable software flow control
        ser.rtscts = False                  #disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False                  #disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 2                #timeout for write

        if (ser.isOpen() == 0):
            ser.open()
            ser.flushInput()
            ser.flushOutput()

        if (ser.isOpen() != 0):

            handshake_status = sensor_node.do_arduino_handshake(ser)

            if (handshake_status[0] == 1):
                print("HANDSHAKE to %s passed" %port)
                conn['path'][port] = dict()
                conn['path'][port]['ser'] = ser
                conn['path'][port]['ser_open'] = int(1)
                conn['path'][port]['handshake_status'] = int(handshake_status[0])
                conn['path'][port]['device_id'] = int(handshake_status[1])
                conn['devid'][handshake_status[1]] = port
                db_init(int(handshake_status[1]))
            else:
                print("HANDSHAKE to %s failed " %port)
                ser.close()

    except Exception as e:
        print("failed to connect to %s\n, error %s" %(port, e))

class arduino (object):

    def __init__(self):
        self.lock = threading.Lock()

    def do_arduino_handshake(self, ser):    

        global CONST 

        try:

            self.lock.acquire()

            handshake_done = 0
            device_id_valid = 0
            ser_active = 0
            device_id = 0

            # do a discovery first

            if (ser.isOpen() == 0):
                self.lock.release()
                return (ser_active, device_id)

            for try_count in range (0, CONST['RETRY_COUNT']):

                if (handshake_done == 0):
                    try:
                        op_code_send = (255).to_bytes(1, 'big')
                        count = ser.write(op_code_send)
                        time.sleep(.1)
                    except:
                        print("failed sending data")

                try:
                    recv = ser.readline()
                    op_code_recv = recv.translate(None, b"\r\n").decode()
                    if (int(op_code_recv) == 255):
                        handshake_done = 1
                    time.sleep(.1)
                except:
                    print("failed receiving data")

            if handshake_done == 0:
                self.lock.release()
                print("handshake_done failed\n")
                return (ser_active, device_id)
                    
            try:

                op_code = (OPCODE['DEVICE_ID']).to_bytes(1, 'big')
                count = ser.write(op_code)

            except:
                print("failed sending data")


            for try_count in range (0, CONST['RETRY_COUNT']):

                try:
                    time.sleep(.001)
                    recv = ser.readline()
                    op_code = int.from_bytes(recv, 'big')
                    op_code = recv.translate(None, b"\r\n").decode()

                    if (int(op_code) == 255):
                        continue
                    else:
                        device_id_valid = 1
                        device_id = int(op_code)
                        break;

                except:
                    print("failed receiving data")

            for try_count in range (0, CONST['RETRY_COUNT']):

                try:
                    time.sleep(.001)
                    recv = ser.readline()
                    op_code = int.from_bytes(recv, 'big')
                    op_code = recv.translate(None, b"\r\n").decode()

                    if (int(op_code) == 254):
                        break;

                except:
                    print("failed receiving data")

            self.lock.release()

            if (handshake_done == 1 and device_id_valid == 1):
                ser_active = 1

            return(ser_active,device_id)

        except Exception as e:
            print("error %s" %e)

    def configure_arduino (self, device_id, cmd_list):

        global conn

        try:

            if device_id not in conn['devid']:
                return
            
            ser_path = conn['devid'][device_id]

            conn_params = conn['path'][ser_path]    

            if conn_params['ser_open'] == 0:
                return

            if conn_params['handshake_status'] == 0:
                return

            ser = conn_params['ser']

            self.lock.acquire()

            try:

                op_code = (OPCODE['CONFIGURE']).to_bytes(1, 'big')
                count = ser.write(op_code)
                time.sleep(.001)

                for cmd in cmd_list:
                    op_code = (cmd).to_bytes(1, 'big')
                    count = ser.write(op_code)
                    time.sleep(.001)
            except:
                print("failed sending data")


            for try_count in range (0, CONST['RETRY_COUNT']):

                try:
                    time.sleep(.001)
                    recv = ser.readline()
                    op_code = int.from_bytes(recv, 'big')
                    op_code = recv.translate(None, b"\r\n").decode()

                    if (int(op_code) == 254):
                        break;

                except:
                    print("failed receiving data")

            self.lock.release()        
            return

        except Exception as e:
            print("error %s" %e)

    def monitor_arduino_response (self, device_id):

        global conn
        ack_received = 0;

        status = dict()

        status['state'] = "offline"

        try:

            if device_id not in conn['devid']:
                return status

            ser_path = conn['devid'][device_id]
            
            conn_params = conn['path'][ser_path]    

            if conn_params['ser_open'] == 0:
                return status

            if conn_params['handshake_status'] == 0:
                return status

            ser = conn_params['ser']

            self.lock.acquire()

            status['state'] = "online"
            status['status'] = []

            while 1:    

                try:
                    time.sleep(.001)
                    recv = ser.readline()
                    op_code = int.from_bytes(recv, 'big')
                    op_code = recv.translate(None, b"\r\n").decode()

                    if (int(op_code) == 254):
                        break
                    else:     
                        status['status'].append(op_code)
                except Exception as e:
                    print("failed receiving data, e %s" %e)


            self.lock.release()        
            return status

        except Exception as e:
            print("error %s" %e)
            return status

    def query_arduino (self, device_id, cmd_list):

        global conn
        ack_received = 0;

        status = dict()

        status['state'] = "offline"

        try:

            if device_id not in conn['devid']:
                return status

            ser_path = conn['devid'][device_id]
            
            conn_params = conn['path'][ser_path]    

            if conn_params['ser_open'] == 0:
                return status

            if conn_params['handshake_status'] == 0:
                return status

            ser = conn_params['ser']

            self.lock.acquire()

            try:

                op_code = (OPCODE['STATUS']).to_bytes(1, 'big')
                count = ser.write(op_code)
                time.sleep(.001)

                for cmd in cmd_list:
                    op_code = (cmd).to_bytes(1, 'big')
                    count = ser.write(op_code)
                    time.sleep(.001)
            except:
                print("failed sending data")
                self.lock.release()        
                return status

            for try_count in range (0, CONST['RETRY_COUNT']):

                try:
                    time.sleep(.001)
                    recv = ser.readline()
                    op_code = int.from_bytes(recv, 'big')
                    op_code = recv.translate(None, b"\r\n").decode()

                    if (int(op_code) == 254):
                        ack_received = 1
                        break;

                except Exception as e:
                    print("failed receiving data, e %s" %e)

            if (ack_received == 0):
                self.lock.release()        
                return status

            status['state'] = "online"
            status['status'] = []

            while 1:    

                try:
                    time.sleep(.001)
                    recv = ser.readline()
                    op_code = int.from_bytes(recv, 'big')
                    op_code = recv.translate(None, b"\r\n").decode()

                    if (int(op_code) == 254):
                        break
                    else:     
                        status['status'].append(op_code)

                except Exception as e:
                    print("failed receiving data, e %s" %e)

            self.lock.release()        
            return status

        except Exception as e:
            print("error %s" %e)
            return status

def query_temp_db (sensor_node, device):

    global device_map
    node_id = 1

    cmd_list = []

    try:

        device_id = "device_%d" %device[0]
        device_pin = int(device_map['TEMP_DB'][device_id])

        cmd_list.append(CONST['TEMP_DB'])

        status = sensor_node.query_arduino(node_id, cmd_list)

        status['id'] = device[0]

        return status

    except Exception as e:
        print("error %s" %e)

def query_light_db (sensor_node, device):

    global device_map
    node_id = 1

    cmd_list = []

    try:

        device_id = "device_%d" %device[0]
        device_pin = int(device_map['LIGHT_DB'][device_id])

        cmd_list.append(CONST['LIGHT_DB'])
        cmd_list.append(device_pin)

        status = sensor_node.query_arduino(node_id, cmd_list)

        status['id'] = device[0]

        return status

    except Exception as e:
        print("error %s" %e)

def query_access_db (sensor_node, device):

    node_id = 2
    sensor_id = device[9]

    cmd_list = []

    try:

        cmd_list.append(CONST['ACCESS_DB'])
        cmd_list.append(int(sensor_id))

        status = sensor_node.query_arduino(node_id, cmd_list)

        status['id'] = device[0]

        return status

    except Exception as e:
        print("error %s" %e)

def configure_light_db (sensor_node, device_list):

    global device_map
    node_id = 1

    try:

        for device in device_list:

            cmd_list = []
            device_id = "device_%d" %device[0]
            device_pin = int(device_map['LIGHT_DB'][device_id])

            cmd_list.append(CONST['LIGHT_DB'])
            cmd_list.append(device_pin)

            if (device[3] == "auto"):
                cmd_list.append(int(1))
            else:
                cmd_list.append(int(2))

            if (device[4] == "on"):
                cmd_list.append(int(1))
            else:
                cmd_list.append(int(0))

            sensor_node.configure_arduino(node_id, cmd_list)

            time.sleep(1)

    except Exception as e:
        print("error %s" %e)

def configure_temp_db (sensor_node, device_list):

    global device_map
    node_id = 1

    try:

        for device in device_list:

            cmd_list = []
            device_id = "device_%d" %device[0]
            device_pin = int(device_map['TEMP_DB'][device_id])

            cmd_list.append(CONST['TEMP_DB'])

            cmd_list.append(int(device[4]))

            sensor_node.configure_arduino(node_id, cmd_list)

            time.sleep(1)

    except Exception as e:
        print("error %s" %e)

def configure_access_db (sensor_node, device_list, db):

    global device_map
    node_id = 2

    try:

        for device in device_list:

            print("------------------------------------- CONFIGURE -----------------------------------")                      
            print(device)

            if (device[12] == '1'): # notify DB
                print("------------------------------------- DB NOTIFY -----------------------------------")                      
                num_active_users = get_num_active_occupants(db)

                if (num_active_users == 0):
                    notify_nodes_of_user_exit(sensor_node)
                else:    
                    notify_nodes_of_user_entry(sensor_node)

#            db_cmd = "UPDATE ACCESS_DB set UPDATED='0',NOTIFY_DB='0' where "
                db_cmd = "UPDATE ACCESS_DB set NOTIFY_DB='0' where ID=%d" %device[0]
                cursor = db.execute(db_cmd)
                db.commit()


            elif (device[10] == '1'): # sync DB

                print("------------------------------------- DB SYNC -----------------------------------")                      
                sensor_id = device[9]
                uid_key = device[4].split("-")
                uid_size = len(uid_key)
                cmd_list = []

                cmd_list.append(CONST['ACCESS_DB'])
                cmd_list.append(OPCODE['ACCESS_DB_SYNC'])
                cmd_list.append(int(sensor_id))
                cmd_list.append(int(uid_size))

                for id in uid_key:
                    cmd_list.append(int(id))

                cmd_list.append(int(device[5]))

                sensor_node.configure_arduino(node_id, cmd_list)

                num_active_users = get_num_active_occupants(db)

                if (num_active_users == 0):
                    notify_nodes_of_user_exit(sensor_node)
                else:    
                    notify_nodes_of_user_entry(sensor_node)

#            db_cmd = "UPDATE ACCESS_DB set UPDATED='0',SYNC='0' where ID=%d" %device[0]
                db_cmd = "UPDATE ACCESS_DB set SYNC='0' where ID=%d" %device[0]
                cursor = db.execute(db_cmd)
                db.commit()

            elif (device[2] == '1'): # key sense

                print("------------------------------------- DB KEY SENSE -----------------------------------")                      
                # add key

                cmd_list = []

                cmd_list.append(CONST['ACCESS_DB'])
                cmd_list.append(OPCODE['ACCESS_DB_SENSE'])

                sensor_node.configure_arduino(node_id, cmd_list)

                status_return = sensor_node.monitor_arduino_response (node_id)

                if (status_return['state'] == "online"):

                    status = status_return['status']

                    if (status[0] != '1'):
                        continue;

                    key_index = status[1]
                    uid_count = int(status[2])    

                    uid = []

                    for id in range (0,uid_count):
                        id_index = int(3 + id)
                        uid.append(status[id_index])

                    uid_key = "-".join(uid)

#                db_cmd = "UPDATE ACCESS_DB set USER_KEY='%s',SENSOR_ID='%s',LEARN_KEY='0',UPDATED='0' where ID=%d" %(uid_key, key_index, device[0])
                    db_cmd = "UPDATE ACCESS_DB set USER_KEY='%s',SENSOR_ID='%s',LEARN_KEY='0' where ID=%d" %(uid_key, key_index, device[0])
                    cursor = db.execute(db_cmd)
                    db.commit()

            elif (device[8] == '1'): # db delete

                print("------------------------------------- DB DELETE -----------------------------------")                      

                cmd_list = []
                sensor_id = device[9]

                cmd_list.append(CONST['ACCESS_DB'])
                cmd_list.append(OPCODE['ACCESS_DB_DELETE'])
                cmd_list.append(int(sensor_id)) #user key status

                sensor_node.configure_arduino(node_id, cmd_list)
                    
                db_cmd = "DELETE FROM ACCESS_DB where ID=%d" %(device[0])
                cursor = db.execute(db_cmd)
                db.commit()

            else:   # activate deactivate

                print("------------------------------------- ACTIVE - DE-ACTIVATE -----------------------------------")                      

                cmd_list = []
                sensor_id = device[9]

                cmd_list.append(CONST['ACCESS_DB'])

                if (device[5] == '1'): # key activated
                    cmd_list.append(OPCODE['ACCESS_DB_ACTIVATE'])
                else: # key deactivated
                    cmd_list.append(OPCODE['ACCESS_DB_DEACTIVATE'])

                cmd_list.append(int(sensor_id)) #user key status

                sensor_node.configure_arduino(node_id, cmd_list)

                db_cmd = "UPDATE ACCESS_DB set UPDATED='0' where ID=%d" %device[0]
                cursor = db.execute(db_cmd)
                db.commit()

            time.sleep(1)

    except Exception as e:
        print("error %s" %e)

def db_monitor_thread_run (sensor_node):

    db_path = "/autohome/db/autoHome.db"


    while 1:

        try:

            time.sleep(1)

            device_list = []

            db = sqlite3.connect(db_path)
            db_cmd = "SELECT * from LIGHT_DB where UPDATED='1'"
            cursor = db.execute(db_cmd)

            for row in cursor:
                device_list.append(row)

            db_cmd = "UPDATE LIGHT_DB set UPDATED='0'"
            cursor = db.execute(db_cmd)
            db.commit()
            db.close()

            configure_light_db(sensor_node, device_list)

            time.sleep(1)

            device_list = []
            db = sqlite3.connect(db_path)

            db_cmd = "SELECT * from TEMP_DB where UPDATED='1'"
            cursor = db.execute(db_cmd)

            for row in cursor:
                device_list.append(row)

            configure_temp_db(sensor_node, device_list)

            db_cmd = "UPDATE TEMP_DB set UPDATED='0'"
            cursor = db.execute(db_cmd)
            db.commit()
            db.close()

            device_list = []
            db = sqlite3.connect(db_path)

            num_active_occupants_before = get_num_active_occupants(db)

            db_cmd = "SELECT * from ACCESS_DB where UPDATED='1'"
            cursor = db.execute(db_cmd)

            for row in cursor:
                device_list.append(row)

            configure_access_db(sensor_node, device_list, db)

            num_active_occupants_after = get_num_active_occupants(db)

            if (num_active_occupants_before == 0 and num_active_occupants_after != 0):
                # new user addition
                notify_nodes_of_user_entry(sensor_node)
                
            if (num_active_occupants_before != 0 and num_active_occupants_after == 0):    
                # user going off
                notify_nodes_of_user_exit(sensor_node)

            db.close()

        except Exception as e:
            print("error %s" %e)

def arduino_monitor_thread_run (sensor_node):

    global CONST

    db_path = "/autohome/db/autoHome.db"

    while 1:

        try:

            time.sleep(1)

            device_list = []
            device_status_list = []

            db = sqlite3.connect(db_path)
            db_cmd = "SELECT * from LIGHT_DB"
            cursor = db.execute(db_cmd)

            for row in cursor:
                device_list.append(row)

            db.close()

            for device in device_list:
                device_status = query_light_db(sensor_node, device)
                device_status_list.append(device_status)

            db = sqlite3.connect(db_path)

            for device_status in device_status_list:

                if (device_status['state'] == "online"):

                    status = device_status['status']
                    
                    if (int(status[0]) == CONST['MODE_AUTO']):
                        mode = "auto"
                    else:    
                        mode = "manual"

                    if (int(status[1]) == 1):
                        state = "on"    
                    else:    
                        state = "off"    

                    db_cmd = "UPDATE LIGHT_DB set DEVICE_STATUS='up',STATUS_MODE='%s',STATUS='%s' where ID=%d" %(mode, state, device_status['id'])
                    db.execute(db_cmd)

                else:
                    db_cmd = "UPDATE LIGHT_DB set DEVICE_STATUS='down' where ID=%d" %device_status['id']
                    db.execute(db_cmd)

            db.commit()
            db.close()

            device_list = []
            device_status_list = []

            db = sqlite3.connect(db_path)
            db_cmd = "SELECT * from TEMP_DB"
            cursor = db.execute(db_cmd)

            for row in cursor:
                device_list.append(row)

            db.close()

            for device in device_list:
                device_status = query_temp_db(sensor_node, device)
                device_status_list.append(device_status)

            db = sqlite3.connect(db_path)

            for device_status in device_status_list:

                if (device_status['state'] == "online"):

                    status = device_status['status']

                    configured_temp = status[0]
                    current_temp = status[1]

                    if (int(status[2]) == 1):
                        ac_status = "on"    
                    else:    
                        ac_status = "off"    
                    
                    db_cmd = "UPDATE TEMP_DB set DEVICE_STATUS='up',STATUS_TEMP='%s',STATUS='%s' where ID=%d" %(current_temp, ac_status, device_status['id'])
                    db.execute(db_cmd)
                else:
                    db_cmd = "UPDATE TEMP_DB set DEVICE_STATUS='down' where ID=%d" %device_status['id']
                    db.execute(db_cmd)

            db.commit()
            db.close()

            num_active_occupants_before = 0
            num_active_occupants_after  = 0
            device_list = []
            device_status_list = []

            db = sqlite3.connect(db_path)
            db_cmd = "SELECT * from ACCESS_DB"
            cursor = db.execute(db_cmd)

            for row in cursor:
                learning_in_progress = row[2]
                if (learning_in_progress == '1'):
                    print("learning in progress")
                    continue

                device_list.append(row)

            num_active_occupants_before = get_num_active_occupants(db)

            db.close()

            for device in device_list:
                device_status = query_access_db(sensor_node, device)
                device_status_list.append(device_status)

            db = sqlite3.connect(db_path)

            for device_status in device_status_list:

                try:
                    if (device_status['state'] == "online"):

                        status = device_status['status']

                        key_active = status[1]
                        user_status = status[2]

                        db_cmd = "UPDATE ACCESS_DB set DEVICE_STATUS='up',SENSOR_KEY_STATUS='%s',USER_STATUS='%s' where ID=%d" %(key_active, user_status, device_status['id'])
                        db.execute(db_cmd)

                    else:
                        db_cmd = "UPDATE ACCESS_DB set DEVICE_STATUS='down' where ID=%d" %device_status['id']
                        db.execute(db_cmd)
                except Exception as e:
                    print("something failed, error %s" %e)

            num_active_occupants_after = get_num_active_occupants(db)

            if (num_active_occupants_before == 0 and num_active_occupants_after != 0):
                # new user addition
                notify_nodes_of_user_entry(sensor_node)
                
            if (num_active_occupants_before != 0 and num_active_occupants_after == 0):    
                # user going off
                notify_nodes_of_user_exit(sensor_node)

            db.commit()
            db.close()

        except Exception as e:
            print("error %s" %e)

def connection_monitor_thread_run (sensor_node):
    
    global conn

    print("connection_monitor_thread_run\n")
    
    while 1:

        time.sleep(.5)

        try:

            glist = []
            new_serial_list = []
            rem_serial_list = []
            glist = glob.glob('/dev/ttyACM*')

            # mark all connections 
            for serials in conn['path']:
                conn['path'][serials]['mark'] = int(1)

            #unmark    
            for serials in glist:
                if serials in conn['path']:
                    conn['path'][serials]['mark'] = int(0)
                else:
                    new_serial_list.append(serials)

            #sweep
            for serials in conn['path']:
                if conn['path'][serials]['mark'] == 1:
                    print("remove %s\n" %serials)
                    # device lost connection
                    ser = conn['path'][serials]['ser']
                    ser.close()
                    device_id = conn['path'][serials]['device_id']
                    rem_serial_list.append(serials)
                    conn['devid'].pop(device_id)

            for serials in rem_serial_list:
                conn['path'].pop(serials)

            for serials in new_serial_list:
                print("add %s\n" %serials)
                serial_init(serials, sensor_node)

        except Exception as e:
            print("connection_monitor_thread_run failure, error %s\n" %e)

class db_monitor_thread (threading.Thread):
    def __init__(self, threadID, name, sensor_node):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sensor_node = sensor_node
    def run(self):
        print("Starting %s\n" %self.name)
        db_monitor_thread_run(self.sensor_node)

class arduino_monitor_thread (threading.Thread):
    def __init__(self, threadID, name, sensor_node):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sensor_node = sensor_node
    def run(self):
        print("Starting %s\n" %self.name)
        arduino_monitor_thread_run (self.sensor_node)

class connection_monitor_thread (threading.Thread):
    def __init__(self, threadID, name, sensor_node):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sensor_node = sensor_node
    def run(self):
        print("Starting %s\n" %self.name)
        connection_monitor_thread_run(self.sensor_node)

def start_thread (sensor_node):

    threads = []

    # Create new threads
    thread1 = arduino_monitor_thread(1, "arduino_monitor_thread", sensor_node)
    thread2 = db_monitor_thread(2, "db_monitor_thread", sensor_node)
    thread3 = connection_monitor_thread(3, "connection_monitor_thread", sensor_node)

    # Start new Threads
    thread1.start()
    thread2.start()
    thread3.start()

    # Add threads to thread list
    threads.append(thread1)
    threads.append(thread2)
    threads.append(thread3)

    # Wait for all threads to complete
    for t in threads:
        t.join()

    print("Exiting Main Thread")

if __name__ == '__main__':
    sensor_node = arduino()
    server_init()
    start_thread(sensor_node)
