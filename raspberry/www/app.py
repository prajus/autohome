# import the Flask class from the flask module
from flask import Flask, render_template, g
from flask import Flask, render_template, redirect, url_for, request, session
from functools import wraps

import pprint
import random
import os
import sqlite3


pp = pprint.PrettyPrinter(indent=4)

# create the application object
app = Flask(__name__)

# config
app.secret_key = os.urandom(8)

# db connect
@app.before_request
def before_request():
    g.db = sqlite3.connect("/autohome/db/autoHome.db")

@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()

support_matrix = [["Lighting Control", "supported"],
		  ["Temperature Control", "supported"],
		  ["Access Control", "supported"]]

# login required decorator
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap

# route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            session['logged_in'] = True
            session['username'] = request.form['username'] 
            return redirect(url_for('home'))

    return render_template('login.html', error=error, app_name="autoHome")

# use decorators to link the function to a url
@app.route('/')
@login_required
def home(): 
    return render_template('index.html', app_name="autoHome", username=session['username'], feature_list=support_matrix)  # render a template

@app.route('/status')
@login_required
def status (): 
    db = dict()
    db['LIGHT_DB'] = []
    db['TEMP_DB'] = []
    db['ACCESS_DB'] = []
    cursor = g.db.execute("SELECT ID,DEVICE,DEVICE_STATUS,STATUS_MODE,STATUS from LIGHT_DB")
    for row in cursor: 
        db['LIGHT_DB'].append(row)
    cursor = g.db.execute("SELECT ID,DEVICE,DEVICE_STATUS,CONFIG_TEMP,STATUS_TEMP,STATUS from TEMP_DB")
    for row in cursor:
        db['TEMP_DB'].append(row)
    cursor = g.db.execute("SELECT ID,USER_NAME,DEVICE_STATUS, USER_KEY,SENSOR_KEY_STATUS,USER_STATUS from ACCESS_DB")
    for row in cursor:
        if row[0] == 0:
            continue
        db['ACCESS_DB'].append(row)
    return render_template('status.html', app_name="autoHome", username=session['username'], device_list=db)  # render a template

@app.route('/config', methods=['GET', 'POST'])
@login_required
def config (): 

    pp.pprint(request.form)

    if request.method == 'POST':

        for db_data,action in request.form.items():
            db_details = db_data.split("/")

            if (db_details[0] == "ACCESS_DB"): 
                print(db_details)
                if (action == "activate"):
                    db_cmd = "UPDATE %s set USER_KEY_STATUS='1',UPDATED='1' where ID=%s AND LEARN_KEY='0'" %(db_details[0], db_details[1])
                elif (action == "deactivate"):
                    db_cmd = "UPDATE %s set USER_KEY_STATUS='0',UPDATED='1' where ID=%s AND LEARN_KEY='0'" %(db_details[0], db_details[1])
                elif (action == "remove"):    
                    db_cmd = "UPDATE %s set UPDATED='1',DELETED='1' where ID=%s" %(db_details[0], db_details[1])
                else: 
                    db_id = int(1)
                    db_cmd = "SELECT ID from ACCESS_DB"
                    cursor = g.db.execute(db_cmd)
                    for row in cursor:
                        if (row[0] >= db_id):
                            db_id = db_id + 1
                    db_cmd = "INSERT INTO %s (ID,USER_NAME,UPDATED,LEARN_KEY,USER_KEY,USER_KEY_STATUS,USER_STATUS,DELETED) VALUES(%d,'%s', '1','1','null','0','0','0')" %(db_details[0], db_id,action)
                    print(db_cmd)        

                g.db.execute(db_cmd)
                g.db.commit()

            if (db_details[0] == "LIGHT_DB"): 

                if (action == "Auto"):
                    db_cmd = "UPDATE %s set CONFIG_MODE='auto',UPDATED='1' where ID=%s" %(db_details[0], db_details[1])
                else:
                    db_cmd = "UPDATE %s set CONFIG_MODE='manual',CONFIG_STATUS='%s',UPDATED='1' where ID=%s" %(db_details[0], action.lower(), db_details[1])

                g.db.execute(db_cmd)
                g.db.commit()

            if (db_details[0] == "TEMP_DB"): 

                if (db_details[2] == "incr"):
                    db_cmd = "SELECT CONFIG_TEMP from TEMP_DB where ID = %s" %(db_details[1])
                    cursor = g.db.execute(db_cmd)
                    for row in cursor:
                        cur_temp = row[0]
                    
                    temp = int(cur_temp) + int(action)
                
                    db_cmd = "UPDATE %s set CONFIG_TEMP='%s',UPDATED='1' where ID=%s" %(db_details[0], temp, db_details[1])

                else:

                    db_cmd = "UPDATE %s set CONFIG_TEMP='%s',UPDATED='1' where ID=%s" %(db_details[0], action, db_details[1])

                g.db.execute(db_cmd)
                g.db.commit()

    db = dict()
    db['LIGHT_DB'] = []
    db['TEMP_DB'] = []
    db['ACCESS_DB'] = []
    cursor = g.db.execute("SELECT ID,DEVICE,CONFIG_MODE,CONFIG_STATUS from LIGHT_DB")
    for row in cursor: 
        db['LIGHT_DB'].append(row)
    cursor = g.db.execute("SELECT ID,DEVICE,CONFIG_TEMP from TEMP_DB")
    for row in cursor:
        db['TEMP_DB'].append(row)
    cursor = g.db.execute("SELECT ID,USER_NAME,DEVICE_STATUS,USER_KEY,USER_KEY_STATUS,USER_STATUS from ACCESS_DB")
    for row in cursor:
        if row[0] == 0:
            continue
        db['ACCESS_DB'].append(row)
    return render_template('config.html', app_name="autoHome", username=session['username'], device_list=db)  # render a template

@app.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    session.pop('username', None)
    return redirect(url_for('home'))

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(host="192.168.42.1", port=5000, debug=True)
